//[SECTIONS] Dependencies and Modules

const exp = require('express');
const mongoose = require('mongoose');


//[SECTIONS] Server Setup
const port = 5500;
const app = exp();

//[SECTIONS] Routing Components


//[SECTIONS] Server Routes


//[SECTIONS] Database Connection
mongoose.connect('mongodb+srv://dylannn3:abz@cluster0.asi2e.mongodb.net/campstonedb?retryWrites=true&w=majority');

const db = mongoose.connection;
db.on('open',() => {
	console.log(`Database connection established!`)
});


//[SECTIONS] Server Response
app.listen(port,() => {
	console.log(`Listening on port ${port}`);
});

